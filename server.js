
var express 	= require('express');
var app 		= express();
var mongoose 	= require('mongoose');

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/angular-todo');

// Specify Models
var Todo = mongoose.model('Todo', {
	text: String
});

// Configuration
app.configure(function() {
	app.use(express.static(__dirname + '/public'));		// Location of static files
	app.use(express.logger('dev'));						// Because of 'dev' option it will displays a log of all request on the console
	app.use(express.bodyParser());						// Change the HTML POST method
	app.use(express.methodOverride());					// Simulate DELETE and PUT
});

// Routes of our API
app.get('/api/todos', function(req, res) {				// GET all TODOs
	Todo.find(function(err, todos) {
		if(err) {
			res.send(err);
		}
		res.json(todos);
	});
});

app.post('/api/todos', function(req, res) {				// POST creates a whole and returns all after creation
	Todo.create({
		text: req.body.text,
		done: false
	}, function(err, todo){
		if(err) {
			res.send(err);
		}

		Todo.find(function(err, todos) {
			if(err){
				res.send(err);
			}
			res.json(todos);
		});
	});
});

app.delete('/api/todos/:todo', function(req, res) {		// DELETE an ALL-specific and returns all after erasing.
	Todo.remove({
		_id: req.params.todo
	}, function(err, todo) {
		if(err){
			res.send(err);
		}

		Todo.find(function(err, todos) {
			if(err){
				res.send(err);
			}
			res.json(todos);
		});

	})
});

app.get('*', function(req, res) {						// Load a simple HTML view where will our Single App Page be
	res.sendFile('./public/index.html');				// Angular handles the frontend(Angular file)
});

// Listen and runs the server
app.listen(8080, function() {
	console.log('App listening on port 8080');
});

